﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class networkedCameraRig : NetworkBehaviour
{
    public Component[] localComponents;
    public NetworkTransformChild[] networkTransformChildren;
    public NetworkTransform networkTransform;


    void Start ()
    {
		if(!isLocalPlayer)
        {
            print("krill");
            foreach(Component comp in localComponents)
            {
                Destroy(comp);
            }
            networkTransform.sendInterval = 0.1f;
            foreach(NetworkTransformChild net in networkTransformChildren)
            {
                net.sendInterval = 0.1f;
            }
        }
	}
	
	
	void Update ()
    {
		
	}
}
